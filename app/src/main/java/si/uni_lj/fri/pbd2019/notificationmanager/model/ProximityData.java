package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class ProximityData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private String notification_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private int accuracy;

    @DatabaseField
    private double proximity;

    @DatabaseField
    private String label;

    public ProximityData(){}

    public ProximityData(String device_id, long timestamp, int accuracy, double proximity, String label) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.accuracy = accuracy;
        this.proximity = proximity;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public double getProximity() {
        return proximity;
    }

    public void setProximity(double proximity) {
        this.proximity = proximity;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
