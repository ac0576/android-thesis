package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

@DatabaseTable
public class LinearAccData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private String notification_id;

    @DatabaseField
    private double mean;

    @DatabaseField
    private double variance;

    @DatabaseField
    private double mean_crossing_rate;

    @DatabaseField
    private double peak;

    @DatabaseField
    private double energy1;

    @DatabaseField
    private double energy2;

    @DatabaseField
    private double energy3;

    @DatabaseField
    private double energy4;

    @DatabaseField
    private double energy_ratio1;

    @DatabaseField
    private double energy_ratio2;

    @DatabaseField
    private double energy_ratio3;

    @DatabaseField
    private double spectral_entropy;

    public LinearAccData(){}

    public LinearAccData(int id, String device_id, long timestamp, String notification_id, double mean, double variance, double mean_crossing_rate, double peak, double energy1, double energy2, double energy3, double energy4, double energy_ratio1, double energy_ratio2, double energy_ratio3, double spectral_entropy) {
        this.id = id;
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.notification_id = notification_id;
        this.mean = mean;
        this.variance = variance;
        this.mean_crossing_rate = mean_crossing_rate;
        this.peak = peak;
        this.energy1 = energy1;
        this.energy2 = energy2;
        this.energy3 = energy3;
        this.energy4 = energy4;
        this.energy_ratio1 = energy_ratio1;
        this.energy_ratio2 = energy_ratio2;
        this.energy_ratio3 = energy_ratio3;
        this.spectral_entropy = spectral_entropy;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }

    public double getMean() {
        return mean;
    }

    public void setMean(double mean) {
        this.mean = mean;
    }

    public double getVariance() {
        return variance;
    }

    public void setVariance(double variance) {
        this.variance = variance;
    }

    public double getMean_crossing_rate() {
        return mean_crossing_rate;
    }

    public void setMean_crossing_rate(double mean_crossing_rate) {
        this.mean_crossing_rate = mean_crossing_rate;
    }

    public double getPeak() {
        return peak;
    }

    public void setPeak(double peak) {
        this.peak = peak;
    }

    public double getEnergy1() {
        return energy1;
    }

    public void setEnergy1(double energy1) {
        this.energy1 = energy1;
    }

    public double getEnergy2() {
        return energy2;
    }

    public void setEnergy2(double energy2) {
        this.energy2 = energy2;
    }

    public double getEnergy3() {
        return energy3;
    }

    public void setEnergy3(double energy3) {
        this.energy3 = energy3;
    }

    public double getEnergy4() {
        return energy4;
    }

    public void setEnergy4(double energy4) {
        this.energy4 = energy4;
    }

    public double getEnergy_ratio1() {
        return energy_ratio1;
    }

    public void setEnergy_ratio1(double energy_ratio1) {
        this.energy_ratio1 = energy_ratio1;
    }

    public double getEnergy_ratio2() {
        return energy_ratio2;
    }

    public void setEnergy_ratio2(double energy_ratio2) {
        this.energy_ratio2 = energy_ratio2;
    }

    public double getEnergy_ratio3() {
        return energy_ratio3;
    }

    public void setEnergy_ratio3(double energy_ratio3) {
        this.energy_ratio3 = energy_ratio3;
    }

    public double getSpectral_entropy() {
        return spectral_entropy;
    }

    public void setSpectral_entropy(double spectral_entropy) {
        this.spectral_entropy = spectral_entropy;
    }
}
