package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class BluetoothData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private String notification_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private String name;

    @DatabaseField
    private int bt_rssi;

    @DatabaseField
    private String label;

    public BluetoothData(){}

    public BluetoothData(String device_id, long timestamp, String name, int bt_rssi, String label) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.name = name;
        this.bt_rssi = bt_rssi;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBt_rssi() {
        return bt_rssi;
    }

    public void setBt_rssi(int bt_rssi) {
        this.bt_rssi = bt_rssi;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
