package si.uni_lj.fri.pbd2019.notificationmanager.model;

public class MetaAccel {

    private String device_id;
    private long timestamp;
    private String notification_id;

    public MetaAccel(String device_id, long timestamp, String notification_id) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.notification_id = notification_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
