package si.uni_lj.fri.pbd2019.notificationmanager.helper;

import org.jtransforms.fft.DoubleFFT_1D;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import si.uni_lj.fri.pbd2019.notificationmanager.model.AccelerometerData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LinearAccData;

public final class MainHelper {

    public static String formatDuration(long time){
        Date date = new Date(time);
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);
        return formatter.format(date);
    }

    public static String getRandomNotificationId(){
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static boolean greaterThan1Min(long time1, long time2){
        return Math.abs(time1 - time2) >= 60000;
    }

    public static LinearAccData accDataProcessing(ArrayList<AccelerometerData> readings){
        //https://bitbucket.org/veljkop/intelligenttrigger/src/master/interruptme/src/main/java/org/ubhave/intelligenttrigger/learners/ActivityLearner.java
        //Line: 201

        LinearAccData res = new LinearAccData();
        int length = readings.size();
        int sk1 = 0, sk2 = 0, sk3 = 0, sk4 = 0;

        double mean = 0;
        double variance = 0;
        double MCR = 0;
        double SP = 0;
        double energy = 0;
        double pSpec = 0;
        double totalIntensity = 0;
        double entropy = 0;
        double fs = length / 60.0;
        double sampleRate = fs / length;
        double eSpec1 = 0;
        double eSpec2 = 0;
        double eSpec3 = 0;
        double eSpec4 = 0;

        double [] intensityList = new double[readings.size()];
        double [] powerSpec = new double[readings.size()];
        double [] probabilityDist = new double[readings.size()];
        double [] combined = new double[readings.size()*2];

        DoubleFFT_1D fft = new DoubleFFT_1D(length);

        for (int i=0; i<readings.size(); i++) {
            double[] sample = {readings.get(i).getX(), readings.get(i).getY(), readings.get(i).getZ()};
            double intensity = (double) Math.sqrt(Math.pow(sample[0], 2) + Math.pow(sample[1], 2) + Math.pow(sample[2], 2));
            energy += Math.pow(intensity, 2);

            intensityList[i] = intensity;
            combined[i] = intensity;
            totalIntensity += intensity;
        }
        if (readings.size() > 0) {
            fft.realForwardFull(combined);
            mean = totalIntensity/readings.size();

            for (int i=0; i<readings.size(); i++){
                variance += Math.pow(intensityList[i] - mean, 2);
                double ps = (double) Math.abs(Math.pow(combined[i*2], 2) - Math.pow(combined[i*2+1], 2));
                double curPeak = Math.sqrt(ps);

                if(i < 10)
                    curPeak = 0;

                if(i < readings.size()/2){
                    double freq = i * sampleRate;
                    /*if(i == readings.size()/2-1)
                        Log.d("FFT", freq + "Hz" + " sr " + sampleRate + " fs " + fs + " len " + length );*/
                    if(freq <= 1) {
                        eSpec1 += ps;
                        sk1++;
                    }else if(freq <= 3) {
                        eSpec2 += ps;
                        sk2++;
                    }else if(freq <= 5) {
                        eSpec3 += ps;
                        sk3++;
                    }else if(freq <= 16) {
                        eSpec4 += ps;
                        sk4++;
                    }

                }

                powerSpec[i] = ps;
                pSpec += ps;

                if (i>0) {
                    if ((intensityList[i] - mean) * (intensityList[i-1] - mean) < 0){
                        MCR++;
                    }
                }
                if(SP < curPeak)
                    SP = curPeak;
            }

            for (int i=0; i<readings.size(); i++){
                double p = powerSpec[i]/pSpec;
                entropy -= p * (Math.log(p)/Math.log(2));
            }

            variance = variance/readings.size();

            if (readings.size() > 1) {
                MCR = MCR/(readings.size()-1);
            }
        }

        res.setMean(mean);
        res.setVariance(variance);
        res.setMean_crossing_rate(MCR);
        res.setPeak(SP);
        res.setEnergy1(eSpec1/sk1);
        res.setEnergy2(eSpec2/sk2);
        res.setEnergy3(eSpec3/sk3);
        res.setEnergy4(eSpec4/sk4);
        res.setEnergy_ratio1(res.getEnergy1()/res.getEnergy2());
        res.setEnergy_ratio2(res.getEnergy3()/res.getEnergy4());
        res.setEnergy_ratio3((res.getEnergy1()+res.getEnergy2())/(res.getEnergy3()+res.getEnergy4()));
        res.setSpectral_entropy(entropy);

        /*Log.d("FFT", "\nMean: " + mean + "\nVariance: " + variance + "\nMCR: " + MCR
        + "\nSP: " + SP + "\neSpec1: " + eSpec1 + "\neSpec2: " + eSpec2 + "\neSpec3: " + eSpec3 + "\neSpec4: " + eSpec4
        + "\nRatio1: " + res.getEnergy_ratio1() + "\nRatio2: " + res.getEnergy_ratio2() + "\nRatio3: " + res.getEnergy_ratio3()
        + "\nEntropy: " + entropy);
*/
        return res;
    }

    public static int powerOf2(int n){
        int power = (int) (Math.log(n)/Math.log(2));
        return (int) Math.pow(2, power);
    }

    public static ArrayList<AccelerometerData> trimArray(ArrayList<AccelerometerData> tbl, int n){
        ArrayList<AccelerometerData> res = new ArrayList<>();
        int n2 = tbl.size() - 2;
        int m2 = n - 2;
        res.add(tbl.get(0));

        int i = 0,j = 0;
        while (j < n2){
            int diff = (i+1) * n2 - (j+1) * m2;
            if(diff < n2/2){
                i++;
                j++;
                res.add(tbl.get(j));
            }else{
                j++;
            }
        }
        res.add(tbl.get(n2+1));
        return res;
    }

    public static boolean specialFilter(String pck, int reason){
        return (pck.equals("com.facebook.orca")
                || pck.equals("com.facebook.katana")
                || pck.equals(("com.facebook.android"))
                || pck.equals("com.instagram.android"))
                && reason == 8;
    }

    public static boolean ignoreFilter(String pck){
        return !(pck.equals("com.android.vending")
                || pck.equals("com.android.providers.downloads"));
    }
}
