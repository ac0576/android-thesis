package si.uni_lj.fri.pbd2019.notificationmanager.worker;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.aware.providers.Battery_Provider;
import com.aware.providers.Bluetooth_Provider;
import com.aware.providers.Light_Provider;
import com.aware.providers.Linear_Accelerometer_Provider;
import com.aware.providers.Locations_Provider;
import com.aware.providers.Proximity_Provider;
import com.aware.providers.Screen_Provider;
import com.aware.providers.WiFi_Provider;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.firestore.FirebaseFirestore;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.List;

import si.uni_lj.fri.pbd2019.notificationmanager.model.AnonId;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BatteryData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BluetoothData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.GoogleActivityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LightData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LinearAccData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LocationData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ProximityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ScreenData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.UserData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.VolumeData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.WiFiData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.config.DatabaseHelper;

public class SyncWorker extends Worker {

    private final static String TAG = "SyncWorker";

    private FirebaseFirestore db;
    private DatabaseReference dbRef;
    private DatabaseHelper databaseHelper;

    private List<LinearAccData> accelerometerData;
    private List<ScreenData> screenDataList;
    private List<BatteryData> batteryDataList;
    private List<GoogleActivityData> googleActivityData;
    private List<LightData> lightDataList;
    private List<LocationData> locationDataList;
    private List<ProximityData> proximityDataList;
    private List<VolumeData> volumeData;
    private List<BluetoothData> bluetoothData;
    private List<WiFiData> wiFiData;

    public SyncWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        FirebaseApp.initializeApp(getApplicationContext());
        db = FirebaseFirestore.getInstance();
        dbRef = FirebaseDatabase.getInstance().getReference();
        databaseHelper = OpenHelperManager.getHelper(getApplicationContext(), DatabaseHelper.class);
        syncData();
        return null;
    }

    private void syncData(){
        getUserData();
        getScreenData();
        getAccelerometerData();
        getBatteryData();
        getBluetoothData();
        getGoogleActivityData();
        getLightData();
        getLocationData();
        getProximityData();
        getVolumeData();
        getWiFiData();
        getAnonId();
    }

    private void getUserData(){
        Dao<UserData, Long> userDao;

        try {
            userDao = databaseHelper.userDao();
            QueryBuilder<UserData, Long> builder = userDao.queryBuilder();
            List<UserData> userDataListr= userDao.query(builder.prepare());
            Log.d(TAG, "Size of workouts: " + userDataListr.size());
            if(userDataListr.size()>0) {
                syncUsers(userDataListr);
                removeUsers();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncUsers (List<UserData> list){
        for(UserData i : list){
            if(i != null)
                dbRef.child("Users").child(i.getNotification_id()).setValue(i);
        }
    }


    private void removeUsers(){
        Dao<UserData, Long> userDao;

        try {
            userDao = databaseHelper.userDao();
            DeleteBuilder<UserData, Long> builder = userDao.deleteBuilder();
            userDao.delete(builder.prepare());
            Log.d(TAG, "Users removed");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getScreenData(){
        Dao<ScreenData, Long> screenDao;

        try {
            screenDao = databaseHelper.screenDao();
            QueryBuilder<ScreenData, Long> builder = screenDao.queryBuilder();
            screenDataList = screenDao.query(builder.prepare());
            Log.d(TAG, "Size of screen data: " + screenDataList.size());
            if(screenDataList.size()>0) {
                syncScreen();
                removeScreenData();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncScreen(){
        if(screenDataList.size()>0)
            for(ScreenData i : screenDataList){
                if(i != null)
                    dbRef.child("Screen").child(i.getNotification_id()).setValue(i);
            }
    }

    private void removeScreenData(){
        Dao<ScreenData, Long> screenDao;

        try {
            screenDao = databaseHelper.screenDao();
            DeleteBuilder<ScreenData, Long> builder = screenDao.deleteBuilder();
            screenDao.delete(builder.prepare());
            Log.d(TAG, "Screen data removed");
            Screen_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getAccelerometerData(){
        Dao<LinearAccData, Long> accelerometerDao;

        try {
            accelerometerDao = databaseHelper.linearAccDao();
            QueryBuilder<LinearAccData, Long> builder = accelerometerDao.queryBuilder();
            accelerometerData = accelerometerDao.query(builder.prepare());
            if(accelerometerData.size()>0) {
                syncAccelerometer();
                removeAccelerometer();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncAccelerometer(){
        for (LinearAccData i : accelerometerData) {
            dbRef.child("Accelerometer").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeAccelerometer(){
        Dao<LinearAccData, Long> accelerometerDao;
        try {
            accelerometerDao = databaseHelper.linearAccDao();
            DeleteBuilder<LinearAccData, Long> builder = accelerometerDao.deleteBuilder();
            accelerometerDao.delete(builder.prepare());
            Log.d(TAG, "Accelerometer data removed");
            Linear_Accelerometer_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getBatteryData(){
        try {
            Dao<BatteryData, Long> batteryDao = databaseHelper.batteryDao();
            QueryBuilder<BatteryData, Long> builder = batteryDao.queryBuilder();
            batteryDataList = batteryDao.query(builder.prepare());
            syncBattery();
            removeBattery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncBattery(){
        for(BatteryData i: batteryDataList){
            if(i != null)
                dbRef.child("Battery").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeBattery(){
        try {
            Dao<BatteryData, Long> batteryDao = databaseHelper.batteryDao();
            DeleteBuilder<BatteryData, Long> builder = batteryDao.deleteBuilder();
            batteryDao.delete(builder.prepare());
            Battery_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getBluetoothData(){
        try {
            Dao<BluetoothData, Long> bluetoothDao = databaseHelper.bluetoothDao();
            QueryBuilder<BluetoothData, Long> builder = bluetoothDao.queryBuilder();
            bluetoothData = bluetoothDao.query(builder.prepare());
            syncBluetooth();
            removeBluetooth();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncBluetooth(){
        for(BluetoothData i: bluetoothData){
            if(i != null)
                dbRef.child("Bluetooth").child(i.getNotification_id())
                    .child(Integer.toString(i.getId())).setValue(i);
        }
    }

    private void removeBluetooth(){
        try {
            Dao<BluetoothData, Long> bluetoothDao = databaseHelper.bluetoothDao();
            DeleteBuilder<BluetoothData, Long> builder = bluetoothDao.deleteBuilder();
            bluetoothDao.delete(builder.prepare());
            Bluetooth_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getGoogleActivityData(){
        try {
            Dao<GoogleActivityData, Long> googleActivityDao = databaseHelper.googleActivityDao();
            QueryBuilder<GoogleActivityData, Long> builder = googleActivityDao.queryBuilder();
            googleActivityData = googleActivityDao.query(builder.prepare());
            syncGoogleActivity();
            removeGoogleActivity();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncGoogleActivity(){
        for(GoogleActivityData i: googleActivityData){
            if(i != null)
                dbRef.child("Activity").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeGoogleActivity(){
        try {
            Dao<GoogleActivityData, Long> googleActivityDao = databaseHelper.googleActivityDao();
            DeleteBuilder<GoogleActivityData, Long> builder = googleActivityDao.deleteBuilder();
            googleActivityDao.delete(builder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getLightData(){
        try {
            Dao<LightData, Long> dao = databaseHelper.lightDao();
            QueryBuilder<LightData, Long> builder = dao.queryBuilder();
            lightDataList = dao.query(builder.prepare());
            syncLight();
            removeLight();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncLight(){
        for(LightData i: lightDataList){
            if(i != null)
                dbRef.child("Light").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeLight(){
        try {
            Dao<LightData, Long> dao = databaseHelper.lightDao();
            DeleteBuilder<LightData, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
            Light_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getLocationData(){
        try {
            Dao<LocationData, Long> dao = databaseHelper.locationDao();
            QueryBuilder<LocationData, Long> builder = dao.queryBuilder();
            locationDataList = dao.query(builder.prepare());
            syncLocation();
            removeLocation();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncLocation(){
        for(LocationData i: locationDataList){
            if(i != null)
                dbRef.child("Location").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeLocation(){
        try {
            Dao<LocationData, Long> dao = databaseHelper.locationDao();
            DeleteBuilder<LocationData, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
            Locations_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getProximityData(){
        try {
            Dao<ProximityData, Long> dao = databaseHelper.proximityDao();
            QueryBuilder<ProximityData, Long> builder = dao.queryBuilder();
            proximityDataList = dao.query(builder.prepare());
            syncProximity();
            removeProximity();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncProximity(){
        for(ProximityData i: proximityDataList){
            if(i != null)
                dbRef.child("Proximity").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeProximity(){
        try {
            Dao<ProximityData, Long> dao = databaseHelper.proximityDao();
            DeleteBuilder<ProximityData, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
            Proximity_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getVolumeData(){
        try {
            Dao<VolumeData, Long> dao = databaseHelper.volumeDao();
            QueryBuilder<VolumeData, Long> builder = dao.queryBuilder();
            volumeData = dao.query(builder.prepare());
            syncVolume();
            removeVolume();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncVolume(){
        for(VolumeData i: volumeData){
            if(i != null)
                dbRef.child("Volume").child(i.getNotification_id()).setValue(i);
        }
    }

    private void removeVolume(){
        try {
            Dao<VolumeData, Long> dao = databaseHelper.volumeDao();
            DeleteBuilder<VolumeData, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getWiFiData(){
        try {
            Dao<WiFiData, Long> dao = databaseHelper.wifiDao();
            QueryBuilder<WiFiData, Long> builder = dao.queryBuilder();
            wiFiData = dao.query(builder.prepare());
            syncWiFi();
            removeWiFi();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncWiFi(){
        for(WiFiData i: wiFiData){
            if(i != null)
                dbRef.child("Wifi").child(i.getNotification_id())
                    .child(Integer.toString(i.getId())).setValue(i);
        }
    }

    private void removeWiFi(){
        try {
            Dao<WiFiData, Long> dao = databaseHelper.wifiDao();
            DeleteBuilder<WiFiData, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
            WiFi_Provider.resetDB(getApplicationContext());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void getAnonId(){
        try {
            Dao<AnonId, Long> dao = databaseHelper.anonIdDao();
            QueryBuilder<AnonId, Long> builder = dao.queryBuilder();
            List<AnonId> temp = dao.query(builder.prepare());
            syncAnonId(temp);
            removeIds();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void syncAnonId(List<AnonId> temp){
        for(AnonId i: temp){
            if(i != null)
                dbRef.child("Ids").child(i.getDevice_id()).setValue(i);
        }
    }

    private void removeIds(){
        try {
            Dao<AnonId, Long> dao = databaseHelper.anonIdDao();
            DeleteBuilder<AnonId, Long> builder = dao.deleteBuilder();
            dao.delete(builder.prepare());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
