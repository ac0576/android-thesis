package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class LightData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private String notification_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private int accuracy;

    @DatabaseField
    private double light_lux;

    @DatabaseField
    private String label;

    public LightData(){}

    public LightData(String device_id, long timestamp, int accuracy, double light_lux, String label) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.accuracy = accuracy;
        this.light_lux = light_lux;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public double getLight_lux() {
        return light_lux;
    }

    public void setLight_lux(double light_lux) {
        this.light_lux = light_lux;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
