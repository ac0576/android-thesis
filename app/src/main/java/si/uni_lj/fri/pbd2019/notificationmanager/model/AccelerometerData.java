package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

@DatabaseTable
public class AccelerometerData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private double x;

    @DatabaseField
    private double y;

    @DatabaseField
    private double z;

    @DatabaseField
    private int accuracy;

    @DatabaseField
    private String notification_id;

    public AccelerometerData(){}

    public AccelerometerData(String device_id, long timestamp, double x, double y, double z, int accuracy, String notification_id) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.x = x;
        this.y = y;
        this.z = z;
        this.accuracy = accuracy;
        this.notification_id = notification_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
