package si.uni_lj.fri.pbd2019.notificationmanager.model.config;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.notificationmanager.R;
import si.uni_lj.fri.pbd2019.notificationmanager.model.AccelerometerData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.AnonId;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BatteryData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BluetoothData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.GoogleActivityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LightData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LinearAccData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LocationData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ProximityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ScreenData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.UserData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.VolumeData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.WiFiData;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper{

    private static final String DATABASE_NAME = "tracker";
    private static final int DATABASE_VERSION = 1;

    /**
     * The data access object used to interact with the Sqlite database to do C.R.U.D operations.
     */
    private Dao<UserData, Long> dataDao;
    private Dao<AccelerometerData, Long> accelerometerDao;
    private Dao<BatteryData, Long> batteryDao;
    private Dao<BluetoothData, Long> bluetoothDao;
    private Dao<LightData, Long> lightDao;
    private Dao<LocationData, Long> locationDao;
    private Dao<ProximityData, Long> proximityDao;
    private Dao<ScreenData, Long> screenDao;
    private Dao<WiFiData, Long> wifiDao;
    private Dao<UserData, Long> userDao;
    private Dao<GoogleActivityData, Long> googleActivityDao;
    private Dao<VolumeData, Long> volumeDao;
    private Dao<AnonId, Long> anonIdDao;
    private Dao<LinearAccData, Long> linearAccDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION,
                /**
                     * R.raw.ormlite_config is a reference to the ormlite_config.txt file in the
                 * /res/raw/ directory of this project
                 * */
                R.raw.ormlite_config);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {

            /**
             * creates the database table
             */
            TableUtils.createTable(connectionSource, AccelerometerData.class);
            TableUtils.createTable(connectionSource, BatteryData.class);
            TableUtils.createTable(connectionSource, BluetoothData.class);
            TableUtils.createTable(connectionSource, LightData.class);
            TableUtils.createTable(connectionSource, LocationData.class);
            TableUtils.createTable(connectionSource, ProximityData.class);
            TableUtils.createTable(connectionSource, ScreenData.class);
            TableUtils.createTable(connectionSource, WiFiData.class);
            TableUtils.createTable(connectionSource, UserData.class);
            TableUtils.createTable(connectionSource, GoogleActivityData.class);
            TableUtils.createTable(connectionSource, VolumeData.class);
            TableUtils.createTable(connectionSource, AnonId.class);
            TableUtils.createTable(connectionSource, LinearAccData.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {
            /**
             * Recreates the database when onUpgrade is called by the framework
             */
            TableUtils.dropTable(connectionSource, AccelerometerData.class, true);
            TableUtils.dropTable(connectionSource, BatteryData.class, true);
            TableUtils.dropTable(connectionSource, BluetoothData.class, true);
            TableUtils.dropTable(connectionSource, LightData.class, true);
            TableUtils.dropTable(connectionSource, LocationData.class, true);
            TableUtils.dropTable(connectionSource, ProximityData.class, true);
            TableUtils.dropTable(connectionSource, ScreenData.class, true);
            TableUtils.dropTable(connectionSource, WiFiData.class, true);
            TableUtils.dropTable(connectionSource, UserData.class, true);
            TableUtils.dropTable(connectionSource, GoogleActivityData.class, true);
            TableUtils.dropTable(connectionSource, VolumeData.class, true);
            TableUtils.dropTable(connectionSource, AnonId.class, true);
            TableUtils.dropTable(connectionSource, LinearAccData.class, true);
            onCreate(database, connectionSource);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<AccelerometerData, Long> accelerometerDao() throws SQLException {
        if(accelerometerDao == null) {
            accelerometerDao = getDao(AccelerometerData.class);
        }
        return accelerometerDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<BatteryData, Long> batteryDao() throws SQLException {
        if(batteryDao == null) {
            batteryDao = getDao(BatteryData.class);
        }
        return batteryDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<BluetoothData, Long> bluetoothDao() throws SQLException {
        if(bluetoothDao == null) {
            bluetoothDao = getDao(BluetoothData.class);
        }
        return bluetoothDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<LightData, Long> lightDao() throws SQLException {
        if(lightDao == null) {
            lightDao = getDao(LightData.class);
        }
        return lightDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<LocationData, Long> locationDao() throws SQLException {
        if(locationDao == null) {
            locationDao = getDao(LocationData.class);
        }
        return locationDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<ProximityData, Long> proximityDao() throws SQLException {
        if(proximityDao == null) {
            proximityDao = getDao(ProximityData.class);
        }
        return proximityDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<ScreenData, Long> screenDao() throws SQLException {
        if(screenDao == null) {
            screenDao = getDao(ScreenData.class);
        }
        return screenDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<WiFiData, Long> wifiDao() throws SQLException {
        if(wifiDao == null) {
            wifiDao = getDao(WiFiData.class);
        }
        return wifiDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<UserData, Long> userDao() throws SQLException {
        if(userDao == null) {
            userDao = getDao(UserData.class);
        }
        return userDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<GoogleActivityData, Long> googleActivityDao() throws SQLException {
        if(googleActivityDao == null) {
            googleActivityDao = getDao(GoogleActivityData.class);
        }
        return googleActivityDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<VolumeData, Long> volumeDao() throws SQLException {
        if(volumeDao == null) {
            volumeDao = getDao(VolumeData.class);
        }
        return volumeDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<AnonId, Long> anonIdDao() throws SQLException {
        if(anonIdDao == null) {
            anonIdDao = getDao(AnonId.class);
        }
        return anonIdDao;
    }

    /**
     * Returns an instance of the data access object
     * @return
     * @throws SQLException
     */
    public Dao<LinearAccData, Long> linearAccDao() throws SQLException {
        if(linearAccDao == null) {
            linearAccDao = getDao(LinearAccData.class);
        }
        return linearAccDao;
    }
}
