package si.uni_lj.fri.pbd2019.notificationmanager;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AppOpsManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.work.Constraints;
import androidx.work.ExistingPeriodicWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.FirebaseApp;

import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;
import si.uni_lj.fri.pbd2019.notificationmanager.service.FirebaseSyncService;
import si.uni_lj.fri.pbd2019.notificationmanager.service.NotificationService;
import si.uni_lj.fri.pbd2019.notificationmanager.worker.SyncWorker;

public class MainActivity extends AppCompatActivity {

    private Button deleteData, syncBtn, crashBtn;
    private boolean consent, locationP, writeP;
    private TextView totalNoti, remNoti, answNoti, debugInfo, debugTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Fabric.with(this, new Crashlytics());

        totalNoti = findViewById(R.id.total_noti_num);
        remNoti = findViewById(R.id.rem_noti_num);
        answNoti = findViewById(R.id.answ_noti_num);
        debugInfo = findViewById(R.id.debug_info_num);
        debugTxt = findViewById(R.id.debug_info);
        deleteData = findViewById(R.id.delete_data);
        syncBtn = findViewById(R.id.sync);

        syncBtn.setVisibility(View.GONE);

        deleteData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

                String text = "I would like my data to be erased.\n\n Phone id: " +
                        Settings.Secure.getString(getContentResolver(),
                        Settings.Secure.ANDROID_ID);

                emailIntent.setType("plain/text");
                emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"ac0576@student.uni-lj.si"});
                emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Request to delete data");
                emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, text);

                startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
        });

        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent syncIntent = new Intent(MainActivity.this, FirebaseSyncService.class);
                syncIntent.setAction(Constants.ACTION_SYNC);
                startService(syncIntent);
            }
        });
        /*
        crashBtn = findViewById(R.id.crash);

        crashBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Crashlytics.getInstance().crash();
            }
        });*/
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    @Override
    protected void onResume() {
        FirebaseApp.initializeApp(this);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        consent = prefs.getBoolean(Constants.CONSENT, false);
        requestForPrecision();

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        totalNoti.setText(Integer.toString(sharedPreferences.getInt(Constants.NOTI_POST, 0)));
        answNoti.setText(Integer.toString(sharedPreferences.getInt(Constants.NOTI_ANSW, 0)));
        remNoti.setText(Integer.toString(sharedPreferences.getInt(Constants.NOTI_RMV, 0)));
        debugInfo.setVisibility(View.GONE);
        debugTxt.setVisibility(View.GONE);
        debugInfo.setText(
                String.format("%d %d", Constants.instances1
                , Constants.instances2));

        Constraints constraints = new Constraints.Builder()
                .setRequiredNetworkType(NetworkType.UNMETERED)
                .setRequiresBatteryNotLow(true)
                .setTriggerContentUpdateDelay(1, TimeUnit.HOURS)
                .build();

        PeriodicWorkRequest saveRequest =
                new PeriodicWorkRequest.Builder(SyncWorker.class, 12, TimeUnit.HOURS)
                        .setConstraints(constraints)
                        .build();

        WorkManager.getInstance()
                .enqueueUniquePeriodicWork(Constants.job_id,
                        ExistingPeriodicWorkPolicy.KEEP, saveRequest);

        super.onResume();
    }

    private void requestForPrecision(){
        NotificationManager nm = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        PowerManager powerManager = (PowerManager) getApplicationContext().getSystemService(POWER_SERVICE);
        boolean granted = false;
        AppOpsManager appOps = (AppOpsManager) this
                .getSystemService(Context.APP_OPS_SERVICE);
        int mode = appOps.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS,
                android.os.Process.myUid(), this.getPackageName());

        if (mode == AppOpsManager.MODE_DEFAULT) {
            granted = (this.checkCallingOrSelfPermission(android.Manifest.permission.PACKAGE_USAGE_STATS) == PackageManager.PERMISSION_GRANTED);
        } else {
            granted = (mode == AppOpsManager.MODE_ALLOWED);
        }
        if(!consent) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.consent_title)
                    .setView(LayoutInflater.from(this).inflate(R.layout.instruction_dialog, null))
                    .setPositiveButton(R.string.consent_agree, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(MainActivity.this);
                            prefs.edit().putBoolean(Constants.CONSENT, true).apply();
                            consent = true;
                            requestForPrecision();
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        }else if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !powerManager.isIgnoringBatteryOptimizations(getPackageName())){
            new AlertDialog.Builder(this)
                    .setTitle(R.string.battery_title)
                    .setMessage(R.string.battery_opt)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            //Prompt the user once explanation has been shown
                            Intent intent = new Intent();
                            intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                            intent.setData(Uri.parse("package:" + getPackageName()));
                            startActivity(intent);
                            dialogInterface.dismiss();
                        }
                    })
                    .create()
                    .show();
        } else {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                new AlertDialog.Builder(this)
                        .setTitle(R.string.title_location_permission)
                        .setMessage(R.string.text_location_permission)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MainActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        Constants.PERMISSIONS_LOCATION_CODE);
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
            }else if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED){
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(this,
                                Manifest.permission.READ_EXTERNAL_STORAGE)
                                != PackageManager.PERMISSION_GRANTED) {

                    new AlertDialog.Builder(this)
                            .setTitle(R.string.store_title)
                            .setMessage(R.string.store_info)
                            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    //Prompt the user once explanation has been shown
                                    String[] PERMISSIONS = {
                                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                            Manifest.permission.READ_EXTERNAL_STORAGE
                                    };
                                    ActivityCompat.requestPermissions(MainActivity.this,
                                            PERMISSIONS,
                                            Constants.PERMISSIONS_WRITE_CODE);
                                    dialogInterface.dismiss();
                                }
                            })
                            .create()
                            .show();
                }
            }else if (nm != null && !nm.isNotificationPolicyAccessGranted()) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.noti_read_title)
                        .setMessage(R.string.noti_info)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                startActivity(new Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS));
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
            } else if(!granted){

                new AlertDialog.Builder(this)
                        .setTitle(R.string.activity_title)
                        .setMessage(R.string.activity_info)
                        .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                                dialogInterface.dismiss();
                            }
                        })
                        .create()
                        .show();
            } else{
                Intent notificationListener = new Intent(this, NotificationService.class);
                startService(notificationListener);
            }
        }
    }


}
