package si.uni_lj.fri.pbd2019.notificationmanager.model;

public class DataAccel {

    private double x;
    private double y;
    private double z;
    private int accuracy;

    public DataAccel(double x, double y, double z, int accuracy) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.accuracy = accuracy;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }
}
