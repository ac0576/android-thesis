package si.uni_lj.fri.pbd2019.notificationmanager;

public abstract class Constants {
    public static final String ACTION_AWARE = "si.uni_lj.fri.pbd2019.notificationmanager.AwareService";
    public static final String BROADCAST_DETECTED_ACTIVITY = "si.uni_lj.fri.pbd2019.notificationmanager.GoogleARAPI";
    static final long DETECTION_INTERVAL_IN_MILLISECONDS = 30 * 1000;
    public static final int CONFIDENCE = 70;
    public static final String DEVICE_ID = "si.uni_lj.fri.pbd2019.notificationmanager.device_id";
    public static final String NOTIFICATION_ACTION = "si.uni_lj.fri.pbd2019.notificationmanager.notification_action";
    public static final String NOTIFICATION_ID = "si.uni_lj.fri.pbd2019.notificationmanager.notification_id";
    public static final String NOTIFICATION_FROM = "si.uni_lj.fri.pbd2019.notificationmanager.notification_from";
    public static final String ACTION_SYNC = "si.uni_lj.fri.pbd2019.notificationmanager.sync";
    public static final String NOTIFICATION_RES_TIME = "si.uni_lj.fri.pbd2019.notificationmanager.notification_res_time";
    public static final String NOTIFICATION_TIME = "si.uni_lj.fri.pbd2019.notificationmanager.action_time";
    public static final String SENSORS_DIS = "si.uni_lj.fri.pbd2019.notificationmanager.sens_dis";
    public static final String NOTIFICATION_PCK = "si.uni_lj.fri.pbd2019.notificationmanager.noti_pck";
    public static final int [] timeIntervals = {2, 4, 54, 2};
    public static final int PERMISSIONS_LOCATION_CODE = 77;
    public static final int PERMISSIONS_WRITE_CODE = 76;
    public static final String schedule_id = "Aware schedule 1";
    public static final String job_id = "notificationmanager.job_id";
    public static final String CONSENT = "si.uni_lj.fri.pbd2019.notificationmanager.consent";
    public static final int ffSzie = 2048;
    public static final String SENSORS_PREF_1 = "si.uni_lj.fri.pbd2019.notificationmanager.sp1";
    public static final String SENSORS_PREF_2 = "si.uni_lj.fri.pbd2019.notificationmanager.sp2";
    public static final String NOTI_ANSW = "si.uni_lj.fri.pbd2019.notificationmanager.notiAns";
    public static final String NOTI_POST = "si.uni_lj.fri.pbd2019.notificationmanager.notiPost";
    public static final String NOTI_RMV = "si.uni_lj.fri.pbd2019.notificationmanager.notiRmv";
    public static int instances1 = 0;
    public static int instances2 = 0;
}
