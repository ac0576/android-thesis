package si.uni_lj.fri.pbd2019.notificationmanager.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.HashMap;

import si.uni_lj.fri.pbd2019.notificationmanager.Constants;
import si.uni_lj.fri.pbd2019.notificationmanager.MainActivity;
import si.uni_lj.fri.pbd2019.notificationmanager.R;
import si.uni_lj.fri.pbd2019.notificationmanager.helper.MainHelper;

public class NotificationService extends NotificationListenerService {
    private final String TAG = this.getClass().getSimpleName();

    private boolean called = false;
    private SharedPreferences sharedPreferences;

    public static final String CHANNEL_ID = "NotificationServiceChannel";
    private static final int [] sensors = new int[3];
    BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(TAG, nRecording-1 + " recordings");
            int i = intent.getIntExtra(Constants.SENSORS_DIS, -1);
            if(i == 2)
                nRecording--;
            Log.d(TAG, "Brod " + i);
            if (i >= 0 && i <= 2) {
                sensors[i]--;
                if (sensors[i] == 0)
                    stopSensors(i);
            }
            if(nRecording == 0){
                stopSensors(0);
                stopSensors(1);
                stopSensors(2);
            }
        }
    };

    private int nRecording = 0;

    private HashMap<String, String> notificationSave = new HashMap<String, String>();
    private HashMap<String, Long> notificationSpam = new HashMap<String, Long>();
    private HashMap<String, Long> notificationRemSpam = new HashMap<String, Long>();
    public NotificationService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.BROADCAST_DETECTED_ACTIVITY);
        LocalBroadcastManager.getInstance(this).registerReceiver(mBroadcastReceiver, filter);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return super.onBind(intent);
    }

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        if(!sbn.getPackageName().equals("android")
                && MainHelper.ignoreFilter(sbn.getPackageName())
                && (!notificationSpam.containsKey(sbn.getPackageName()) ||
                MainHelper.greaterThan1Min(notificationSpam.get(sbn.getPackageName()), sbn.getPostTime()))) {

            /*Log.d(TAG, "Notification created" + sbn.getNotification());
            Log.d(TAG, "Sbn created" + sbn);
            Log.d(TAG, "Time " + sbn.getPostTime());*/

            int total = sharedPreferences.getInt(Constants.NOTI_POST, 0) + 1;
            sharedPreferences.edit().putInt(Constants.NOTI_POST, total).apply();

            if(notificationSpam.containsKey(sbn.getPackageName())){
                notificationRemSpam.remove(sbn.getPackageName());
            }

            notificationSpam.put(sbn.getPackageName(), sbn.getPostTime());
            notificationSave.put(sbn.getPackageName(), MainHelper.getRandomNotificationId());
            Log.d(TAG, "Package is :" + sbn.getPackageName());
            Intent getData = new Intent(this, AwareService.class);
            getData.setAction(Constants.ACTION_AWARE);
            getData.putExtra(Constants.NOTIFICATION_ACTION, "Posted");
            getData.putExtra(Constants.NOTIFICATION_ID, notificationSave.get(sbn.getPackageName()));
            getData.putExtra(Constants.NOTIFICATION_TIME , sbn.getPostTime());
            sensors[0]++;
            sensors[1]++;
            sensors[2]++;
            nRecording++;
            startService(getData);
        }
        super.onNotificationPosted(sbn);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn, RankingMap rankingMap, int reason) {


        called = !MainHelper.specialFilter(sbn.getPackageName(), reason);
        Log.d(TAG, "Reason: " + reason + " Package: " + sbn.getPackageName() + " Called: " + called);
        if(!sbn.getPackageName().equals("android")) {
            long time = System.currentTimeMillis() - sbn.getPostTime();
            if((reason == REASON_CLICK || reason == REASON_CANCEL)
                    && MainHelper.ignoreFilter(sbn.getPackageName())
                    && (!notificationRemSpam.containsKey(sbn.getPackageName()) ||
                    MainHelper.greaterThan1Min(notificationRemSpam.get(sbn.getPackageName())
                            , System.currentTimeMillis()))) {

                called = true;
                notificationRemSpam.put(sbn.getPackageName(), System.currentTimeMillis());

                Intent getData = new Intent(this, AwareService.class);
                getData.setAction(Constants.ACTION_AWARE);
                getData.putExtra(Constants.NOTIFICATION_FROM, notificationSave.get(sbn.getPackageName()));
                getData.putExtra(Constants.NOTIFICATION_ID, MainHelper.getRandomNotificationId());
                getData.putExtra(Constants.NOTIFICATION_RES_TIME, time);
                getData.putExtra(Constants.NOTIFICATION_TIME, System.currentTimeMillis());

                if (reason == REASON_CLICK) {
                    Log.d(TAG, "Clicked" );
                    getData.putExtra(Constants.NOTIFICATION_ACTION, "Clicked");
                    int total = sharedPreferences.getInt(Constants.NOTI_ANSW, 0) + 1;
                    sharedPreferences.edit().putInt(Constants.NOTI_ANSW, total).apply();
                } else {
                    Log.d(TAG, "Removed");
                    getData.putExtra(Constants.NOTIFICATION_ACTION, "Removed");
                    int total = sharedPreferences.getInt(Constants.NOTI_RMV, 0) + 1;
                    sharedPreferences.edit().putInt(Constants.NOTI_RMV, total).apply();
                }
                sensors[0]++;
                sensors[1]++;
                sensors[2]++;
                nRecording++;
                startService(getData);
            }
            notificationSave.remove(sbn.getPackageName());
        }
        super.onNotificationRemoved(sbn, rankingMap, reason);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent != null) {
            sharedPreferences.edit().putInt(Constants.SENSORS_PREF_1, 0).apply();
            sharedPreferences.edit().putInt(Constants.SENSORS_PREF_2, 0).apply();
            String input = intent.getStringExtra("inputExtra");
            createNotificationChannel();
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(this,
                    0, notificationIntent, 0);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                        .setContentTitle(getString(R.string.foreground_title))
                        .setContentText(input)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentIntent(pendingIntent)
                        .setSound(null, AudioManager.STREAM_NOTIFICATION)
                        .build();

                startForeground(1, notification);
            }else{
                Notification notification = new NotificationCompat.Builder(this)
                        .setContentTitle(getString(R.string.foreground_title))
                        .setContentText(input)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentIntent(pendingIntent)
                        .setSound(null, AudioManager.STREAM_NOTIFICATION)
                        .build();

                startForeground(1321, notification);
            }
        }

        //do heavy work on a background thread

        return START_NOT_STICKY;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mBroadcastReceiver != null)
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    private void stopSensors(int i){
        /*switch (i){
            case 0:
                Log.d(TAG, "Brod Dis 1");
                //Aware.stopSensor(this, Aware_Preferences.STATUS_BATTERY);
                break;
            case 1:
                Log.d(TAG, "Brod Dis 2");

               *//* Aware.setSetting(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS, false);
                Aware.startSensor(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS);
                Aware.stopSensor(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS);*//*

                break;
            case 2:
                Log.d(TAG, "Brod Dis 3");
                Aware.stopSensor(this, Aware_Preferences.STATUS_LINEAR_ACCELEROMETER);
                Aware.stopSensor(this, Aware_Preferences.STATUS_BLUETOOTH);
                Aware.stopSensor(this, Aware_Preferences.STATUS_NETWORK_EVENTS);
                Aware.stopSensor(this, Aware_Preferences.STATUS_NETWORK_TRAFFIC);
                Aware.stopSensor(this, Aware_Preferences.STATUS_WIFI);
                break;
        }*/
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn, RankingMap rankingMap) {
        long time = System.currentTimeMillis() - sbn.getPostTime();
        String pck = sbn.getPackageName();
        if(!called) {
            if ((!notificationRemSpam.containsKey(sbn.getPackageName())
                    || MainHelper.greaterThan1Min(notificationRemSpam.get(pck)
                    , System.currentTimeMillis()))
                    && MainHelper.ignoreFilter(pck)) {
                Log.d(TAG, "Here");
                notificationRemSpam.put(sbn.getPackageName(), System.currentTimeMillis());
                Intent getData = new Intent(this, AwareService.class);
                getData.setAction(Constants.ACTION_AWARE);
                getData.putExtra(Constants.NOTIFICATION_FROM, notificationSave.get(sbn.getPackageName()));
                getData.putExtra(Constants.NOTIFICATION_ID, MainHelper.getRandomNotificationId());
                getData.putExtra(Constants.NOTIFICATION_RES_TIME, sbn.getPostTime());
                getData.putExtra(Constants.NOTIFICATION_TIME, sbn.getPostTime());
                getData.putExtra(Constants.NOTIFICATION_ACTION, "");
                getData.putExtra(Constants.NOTIFICATION_PCK, sbn.getPackageName());
                sensors[0]++;
                sensors[1]++;
                sensors[2]++;
                nRecording++;
                startService(getData);
            }
        }
        called = false;
        notificationSave.remove(sbn.getPackageName());
        super.onNotificationRemoved(sbn, rankingMap);
    }
}

