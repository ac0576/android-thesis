package si.uni_lj.fri.pbd2019.notificationmanager.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class ScreenData {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String device_id;

    @DatabaseField
    private String notification_id;

    @DatabaseField
    private long timestamp;

    @DatabaseField
    private String screen_status;

    @DatabaseField
    private String label;

    public ScreenData(){}

    public ScreenData(String device_id, long timestamp, String screen_status, String label) {
        this.device_id = device_id;
        this.timestamp = timestamp;
        this.screen_status = screen_status;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getScreen_status() {
        return screen_status;
    }

    public void setScreen_status(String screen_status) {
        this.screen_status = screen_status;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getNotification_id() {
        return notification_id;
    }

    public void setNotification_id(String notification_id) {
        this.notification_id = notification_id;
    }
}
