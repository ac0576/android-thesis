package si.uni_lj.fri.pbd2019.notificationmanager.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import si.uni_lj.fri.pbd2019.notificationmanager.service.NotificationService;

public class BootCompletedReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //Start foreground service on boot complete
        Intent notificationListener = new Intent(context, NotificationService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(notificationListener);
        }else{
            context.startService(notificationListener);
        }
    }
}
