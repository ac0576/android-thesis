package si.uni_lj.fri.pbd2019.notificationmanager.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

import si.uni_lj.fri.pbd2019.notificationmanager.Constants;
import si.uni_lj.fri.pbd2019.notificationmanager.model.GoogleActivityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.config.DatabaseHelper;

public class ActivityRecognizationIntentService extends IntentService {

    private static String TAG = "ActivityRecognizationIntentService";

    private DatabaseHelper databaseHelper;

    private String deviceId;

    private String notificationId;

    public ActivityRecognizationIntentService() {
        super("ActivityRecognizationIntentService");
    }

    @Override
    public void onCreate() {
        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        super.onCreate();
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Service started");
        if(ActivityRecognitionResult.hasResult(intent)){
            deviceId = intent.getStringExtra(Constants.DEVICE_ID);
            notificationId = intent.getStringExtra(Constants.NOTIFICATION_ID);

            ActivityRecognitionResult activityRecognitionResult = ActivityRecognitionResult.extractResult(intent);
            DetectedActivity detectedActivity = activityRecognitionResult.getMostProbableActivity();

            int confidence = detectedActivity.getConfidence();
            String recognizeActivity = getActivityName(detectedActivity.getType());

            Log.d(TAG,"Confidence : " + confidence);
            Log.d(TAG,"RecognizeActivity : " + recognizeActivity);

            handleResult(recognizeActivity, confidence);
        }
    }

    private String getActivityName(int activityType){
        switch (activityType){
            case DetectedActivity.IN_VEHICLE:
                return "IN_VEHICLE";
            case DetectedActivity.ON_BICYCLE:
                return "ON_BICYCLE";
            case DetectedActivity.STILL:
                return "STILL";
            case DetectedActivity.TILTING:
                return "TILTING";
            case DetectedActivity.RUNNING:
                return "RUNNING";
            case DetectedActivity.ON_FOOT:
                return "ON_FOOT";
            case DetectedActivity.WALKING:
                return "WALKING";
            case DetectedActivity.UNKNOWN:
                return "UNKNOWN";
        }
        return "";
    }


    //Save the result locally
    private void handleResult(String recognizedActivity, int confidence){
        Log.d(TAG, "Device id: " + deviceId + "\nNotification id: "+
                notificationId + "\nActivity: " +recognizedActivity +
                " Confidence: " + confidence);
        GoogleActivityData googleActivityData = new GoogleActivityData();
        googleActivityData.setActivity(recognizedActivity);
        googleActivityData.setConfidence(confidence);
        googleActivityData.setDevice_id(deviceId);
        googleActivityData.setTimestamp(System.currentTimeMillis());
        googleActivityData.setNotification_id(notificationId);

        try {
            Dao<GoogleActivityData, Long> googleActivityDao = databaseHelper.googleActivityDao();
            googleActivityDao.create(googleActivityData);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
