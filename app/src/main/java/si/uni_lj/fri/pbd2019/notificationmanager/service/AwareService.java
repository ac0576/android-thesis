package si.uni_lj.fri.pbd2019.notificationmanager.service;

import android.app.IntentService;
import android.app.KeyguardManager;
import android.app.PendingIntent;
import android.app.usage.UsageEvents;
import android.app.usage.UsageStatsManager;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.aware.Aware;
import com.aware.Aware_Preferences;
import com.aware.providers.Accelerometer_Provider;
import com.aware.providers.Battery_Provider;
import com.aware.providers.Bluetooth_Provider;
import com.aware.providers.Linear_Accelerometer_Provider;
import com.aware.providers.Locations_Provider;
import com.aware.providers.WiFi_Provider;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.ActivityRecognitionClient;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;

import si.uni_lj.fri.pbd2019.notificationmanager.Constants;
import si.uni_lj.fri.pbd2019.notificationmanager.helper.MainHelper;
import si.uni_lj.fri.pbd2019.notificationmanager.model.AccelerometerData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.AnonId;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BatteryData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.BluetoothData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LightData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LinearAccData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.LocationData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ProximityData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.ScreenData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.UserData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.VolumeData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.WiFiData;
import si.uni_lj.fri.pbd2019.notificationmanager.model.config.DatabaseHelper;

public class AwareService extends IntentService implements SensorEventListener {

    private final String TAG = "AwareService";
    private DatabaseHelper databaseHelper;
    private GoogleApiClient.ConnectionCallbacks connectionCallbacks;
    private GoogleApiClient.OnConnectionFailedListener onConnectionFailedListener;
    private GoogleApiClient googleApiClient;
    private SharedPreferences prefs;
    private String deviceId, notificationReason, notification_from, notificationId;
    private Handler handler = new Handler();
    private int index = 0;
    private long time, responseTime;
    private ActivityRecognitionClient activityRecognitionClient;
    private SensorManager mSensorManager;
    private Sensor mSensor;
    private Sensor mLight;
    private double lightV, proxV;
    private boolean cProx = false, cLight = false;
    private SharedPreferences sharedPreferences;
    private String notificationPck;

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if(index < Constants.timeIntervals.length) {
                handler.postDelayed(runnable, Constants.timeIntervals[index] * 1000);
                if(index == 0){
                    Constants.instances1++;
                    Constants.instances2++;
                }
                if(index > 0) {
                    /*if(nRunnables == 1)
                        stopSensors(index - 1);*/
                    if(index == 1) {
                        Intent intent = new Intent(AwareService.this, ActivityRecognizationIntentService.class);
                        intent.putExtra(Constants.DEVICE_ID, deviceId);
                        intent.putExtra(Constants.NOTIFICATION_ID, notificationId);
                        PendingIntent pendingIntent = PendingIntent.getService(AwareService.this,1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
                        activityRecognitionClient.removeActivityUpdates(pendingIntent);
                    }
                    putData(index - 1);
                }
                index++;
            }
        }
    };

    public AwareService() {
        super("AwareService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final String action = intent.getAction();
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        mLight = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        mSensorManager.registerListener(this, mSensor,
                SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mLight,
                SensorManager.SENSOR_DELAY_FASTEST);
        activityRecognitionClient = ActivityRecognition.getClient(AwareService.this);
        notificationReason = intent.getStringExtra(Constants.NOTIFICATION_ACTION);
        notificationId = intent.getStringExtra(Constants.NOTIFICATION_ID);
        responseTime = intent.getLongExtra(Constants.NOTIFICATION_RES_TIME, 0);
        time = intent.getLongExtra(Constants.NOTIFICATION_TIME, 0);
        notification_from = intent.getStringExtra(Constants.NOTIFICATION_FROM);
        notificationPck = intent.getStringExtra(Constants.NOTIFICATION_PCK);

        Log.d(TAG, "Notification id " + notificationId + " Noti pck: " + notificationPck);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if (Constants.ACTION_AWARE.equals(action)
                && notificationReason != null
                && notificationId != null) {

            Log.d(TAG, "Reason: " + notificationReason + " Id: " + notificationId);
            prefs = PreferenceManager.getDefaultSharedPreferences(this);
            getSensorData();
        }
    }

    @Override
    public void onCreate() {
        initGoogleApi();
        databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        super.onCreate();
    }

    //Initialize Google Activity Recognition API
    private void initGoogleApi(){
        connectionCallbacks = new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                Log.d(TAG, "Google API connected");
                Intent intent = new Intent(AwareService.this, ActivityRecognizationIntentService.class);
                intent.putExtra(Constants.DEVICE_ID, deviceId);
                intent.putExtra(Constants.NOTIFICATION_ID, notificationId);
                PendingIntent pendingIntent = PendingIntent.getService(AwareService.this,1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
                ActivityRecognitionClient activityRecognitionClient = ActivityRecognition.getClient(AwareService.this);
                activityRecognitionClient.requestActivityUpdates(61 * 1000, pendingIntent);
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        };

        onConnectionFailedListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

            }
        };

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(ActivityRecognition.API)
                .addConnectionCallbacks(connectionCallbacks)
                .addOnConnectionFailedListener(onConnectionFailedListener)
                .build();
    }

    private void getSensorData(){
        startSensors();


        if (googleApiClient.isConnected())
            googleApiClient.reconnect();
        else
            googleApiClient.connect();

        deviceId = prefs.getString(Constants.DEVICE_ID, "");
        Log.d(TAG, "Device id is " + deviceId);
        if(deviceId.equals("")) {
            String id = MainHelper.getRandomNotificationId();
            Aware.setSetting(this, Aware_Preferences.DEVICE_ID, id);
            prefs.edit().putString(Constants.DEVICE_ID, id).apply();
            deviceId = id;
            putUser();
        }
        runnable.run();



    }

    private void startSensors(){
        Aware.setSetting(this, Aware_Preferences.FREQUENCY_LINEAR_ACCELEROMETER, 0 );
        Aware.setSetting(this, Aware_Preferences.STATUS_LINEAR_ACCELEROMETER,  true);
        Aware.startSensor(this, Aware_Preferences.STATUS_LINEAR_ACCELEROMETER);
        Aware.setSetting(this, Aware_Preferences.STATUS_BATTERY, true);
        Aware.startSensor(this, Aware_Preferences.STATUS_BATTERY);
        Aware.setSetting(this, Aware_Preferences.FREQUENCY_BLUETOOTH, 60);
        Aware.setSetting(this, Aware_Preferences.STATUS_BLUETOOTH, true);
        Aware.startSensor(this, Aware_Preferences.STATUS_BLUETOOTH);
        Aware.setSetting(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS, true);
        Aware.startSensor(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS);
        Aware.startSensor(this, Aware_Preferences.STATUS_NETWORK_EVENTS);
        Aware.setSetting(this, Aware_Preferences.FREQUENCY_NETWORK_TRAFFIC, 60);
        Aware.startSensor(this, Aware_Preferences.STATUS_NETWORK_TRAFFIC);
        Aware.setSetting(this, Aware_Preferences.FREQUENCY_WIFI, 60);
        Aware.startSensor(this, Aware_Preferences.STATUS_WIFI);
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }

    //For testing and debugging
    private void printData(){
        Log.d(TAG, "Printing");
        Cursor bluetooth_data = getContentResolver()
                .query(Bluetooth_Provider.Bluetooth_Data.CONTENT_URI, null, "", null, "timestamp ASC");

        if(bluetooth_data != null && bluetooth_data.moveToFirst()) {
            do {
                String x = bluetooth_data.getString(bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data._ID));
                String y = bluetooth_data.getString(bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data.BT_NAME));
                String z = bluetooth_data.getString(bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data.BT_LABEL));
                String deviceId = bluetooth_data.getString(bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data.DEVICE_ID));
                long time = bluetooth_data.getLong(bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data.TIMESTAMP));
                //do what you need with x,y,z variables
                Log.d(TAG, String.format("Id:%s Device_Id:%s BtName:%s BrLabel:%s unfiltered: %d time: %s", x,
                        deviceId, y, z, time, MainHelper.formatDuration(time)));
            } while (bluetooth_data.moveToNext());
        }
    }

    //Save info into a local DB about this session
    private void setSessionData(){
        UserData userData = new UserData();
        Log.d(TAG, "User id");
        userData.setDevice_id(deviceId);
        userData.setNotification_action(notificationReason);
        userData.setTimestamp(time);
        userData.setNotification_id(notificationId);
        userData.setResponse_time(responseTime);
        userData.setNotification_from(notification_from);

        try {
            Dao<UserData, Long> userData1 = databaseHelper.userDao();
            userData1.create(userData);
            Log.d(TAG,"User added");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void putData(int i){
        switch (i){
            case 0:
                putBatteryData();
                putProximityData();
                putLightData();
                putVolumeData();
                putScreenData();
                googleApiClient.disconnect();
                break;
            case 1:
                int sens1 = sharedPreferences.getInt(Constants.SENSORS_PREF_1, 0);
                Log.d(TAG, "Num of 1 instances: " + Constants.instances1);
                sens1--;
                sharedPreferences.edit().putInt(Constants.SENSORS_PREF_1, sens1).apply();
                Constants.instances1--;
                if(Constants.instances1 == 0){
                    Aware.setSetting(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS, false);
                    Aware.startSensor(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS);
                    Aware.stopSensor(getApplicationContext(), Aware_Preferences.STATUS_LOCATION_GPS);
                }
                putLocationData();
                broadcast(i);
                break;
            case 2:
                int sens2 = sharedPreferences.getInt(Constants.SENSORS_PREF_1, 0);
                Log.d(TAG, "Num of 2 instances: " + Constants.instances2);
                sens2--;
                sharedPreferences.edit().putInt(Constants.SENSORS_PREF_2, sens2).apply();
                Constants.instances2--;
                if(Constants.instances2 == 0){
                    Aware.stopSensor(this, Aware_Preferences.STATUS_LINEAR_ACCELEROMETER);
                    Aware.stopSensor(this, Aware_Preferences.STATUS_BLUETOOTH);
                    Aware.stopSensor(this, Aware_Preferences.STATUS_NETWORK_EVENTS);
                    Aware.stopSensor(this, Aware_Preferences.STATUS_NETWORK_TRAFFIC);
                    Aware.stopSensor(this, Aware_Preferences.STATUS_WIFI);
                }
                putAccelerometerData();
                BluetoothData();
                putWiFiData();
                getRecentActivity(this, time);
                break;
            default:
                break;
        }
    }

    private void putLocationData(){
        LocationData tmp = new LocationData();
        Cursor location_data = getContentResolver()
                .query(Locations_Provider.Locations_Data.CONTENT_URI,
                        null, "timestamp > " + (time - 5000),
                        null, "accuracy DESC");
        if(location_data != null) {
            if (location_data.moveToFirst()) {
                tmp.setDevice_id(deviceId);
                tmp.setTimestamp(time);
                tmp.setNotification_id(notificationId);
                tmp.setLatitude(location_data.getDouble(
                        location_data.getColumnIndex(Locations_Provider.Locations_Data.LATITUDE)));
                tmp.setLongitude(location_data.getDouble(
                        location_data.getColumnIndex(Locations_Provider.Locations_Data.LONGITUDE)));

                try {
                    Dao<LocationData, Long> locationDao = databaseHelper.locationDao();
                    locationDao.create(tmp);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void putLightData(){
        LightData tmp = new LightData();
        tmp.setDevice_id(deviceId);
        tmp.setTimestamp(time);
        tmp.setNotification_id(notificationId);
        tmp.setLight_lux(lightV);
        try {
            Dao<LightData, Long> lightDao = databaseHelper.lightDao();
            lightDao.create(tmp);
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    private void putProximityData(){
        ProximityData tmp = new ProximityData();
        tmp.setDevice_id(deviceId);
        tmp.setTimestamp(time);
        tmp.setNotification_id(notificationId);
        if(proxV == 0)
            tmp.setProximity(proxV);
        else
            tmp.setProximity(1);
        try {
            Dao<ProximityData, Long> proximityDao = databaseHelper.proximityDao();
            proximityDao.create(tmp);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    //0 - Covered
    //1 - Not covered
    private void putScreenData(){
        ScreenData tmp = new ScreenData();

        KeyguardManager myKM = (KeyguardManager) this.getSystemService(Context.KEYGUARD_SERVICE);
        tmp.setDevice_id(deviceId);
        tmp.setTimestamp(time);
        tmp.setNotification_id(notificationId);
        if( myKM.inKeyguardRestrictedInputMode()) {
            //Locked
            tmp.setScreen_status("Locked");
        } else {
            //Unlocked
            tmp.setScreen_status("Unlocked");
        }

        try {
            Dao<ScreenData, Long> screenDao = databaseHelper.screenDao();
            screenDao.create(tmp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //0 - Sound
    //1 - Vibrate
    //2 - Silent
    //4 - DND
    private void putVolumeData(){
        VolumeData tmp = new VolumeData();
        tmp.setDevice_id(deviceId);
        tmp.setTimestamp(time);
        tmp.setNotification_id(notificationId);

        AudioManager myAudioManager = (AudioManager)getSystemService(Context.AUDIO_SERVICE);

        if(myAudioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL){
            tmp.setState(0);
        }else if(myAudioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE){
            tmp.setState(1);
        }else if(myAudioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT){
            tmp.setState(2);
        }

        try {
            if(Settings.Global.getInt(getContentResolver(), "zen_mode") != 0){
                //DND enabled
                Log.d(TAG, "DND ENABLED");
                tmp.setState(4);
            }
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Dao<VolumeData, Long> volumeDao = databaseHelper.volumeDao();
            volumeDao.create(tmp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Get accelerometer data from AWARE
    private void putAccelerometerData(){
        ArrayList<AccelerometerData> accelerometerData = new ArrayList<>();
        Cursor accelerometer_data = getContentResolver()
                .query(Linear_Accelerometer_Provider.Linear_Accelerometer_Data.CONTENT_URI,
                        null, "timestamp > " + time, null,
                        "timestamp ASC");
        long diff = -1;
        if(accelerometer_data != null && accelerometer_data.moveToFirst()) {
            do {
                AccelerometerData tempAccData = new AccelerometerData();
                tempAccData.setDevice_id(deviceId);
                tempAccData.setNotification_id(notificationId);

                tempAccData.setTimestamp(
                        accelerometer_data.getLong(
                                accelerometer_data.getColumnIndex(
                                        Accelerometer_Provider.Accelerometer_Data.TIMESTAMP)));

                tempAccData.setX(
                        accelerometer_data.getDouble(
                                accelerometer_data.getColumnIndex(
                                        Linear_Accelerometer_Provider.Linear_Accelerometer_Data.VALUES_0)));
                tempAccData.setY(
                        accelerometer_data.getDouble(
                                accelerometer_data.getColumnIndex(
                                        Linear_Accelerometer_Provider.Linear_Accelerometer_Data.VALUES_1)));
                tempAccData.setZ(
                        accelerometer_data.getDouble(
                                accelerometer_data.getColumnIndex(
                                        Linear_Accelerometer_Provider.Linear_Accelerometer_Data.VALUES_2)));
                accelerometerData.add(tempAccData);
            } while (accelerometer_data.moveToNext());

            Log.d(TAG, "Accelerometer data " + accelerometerData.size() + " samples");
            if(accelerometerData.size() >= 2048) {
                accelerometerData = MainHelper.trimArray(accelerometerData, Constants.ffSzie);
            /*String root = Environment.getExternalStorageDirectory().toString();
            File myDir = new File(root + "/saved_images");

            StringBuilder xArray = new StringBuilder();
            StringBuilder yArray = new StringBuilder();
            StringBuilder zArray = new StringBuilder();

            for(AccelerometerData i : accelerometerData){
                xArray.append(i.getX()).append(", ");
                yArray.append(i.getY()).append(", ");
                zArray.append(i.getZ()).append(", ");
            }

            if (!myDir.exists()) {
                myDir.mkdirs();
            }
            File file = new File (myDir, "AccData");
            if (file.exists ())
                file.delete ();
            try {
                FileOutputStream fileOutput = new FileOutputStream(file);
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutput);
                outputStreamWriter.write("X = [" + xArray + "]\nY = [" + yArray + "]\nZ = [" + zArray + "]");
                outputStreamWriter.close();
            }
            catch (IOException e) {
                Log.e("Exception", "File write failed: " + e.toString());
            }*/
//            Log.d(TAG, "Accelerometer trimmed data " + accelerometerData.size() + " samples");
                LinearAccData data = MainHelper.accDataProcessing(accelerometerData);
                data.setDevice_id(deviceId);
                data.setTimestamp(time);
                data.setNotification_id(notificationId);

                try {
                    Dao<LinearAccData, Long> accelerometerDao = databaseHelper.linearAccDao();
                    accelerometerDao.create(data);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //2 - Charging
    //3 - Discharging
    private void putBatteryData(){
        Aware.stopSensor(this, Aware_Preferences.STATUS_BATTERY);
        Cursor battery_data = getContentResolver()
                .query(Battery_Provider.Battery_Data.CONTENT_URI,
                        null, "timestamp > " + time,
                        null, "timestamp DESC");

        BatteryData tmp = new BatteryData();
        tmp.setDevice_id(deviceId);
        tmp.setTimestamp(time);
        tmp.setNotification_id(notificationId);

        if(battery_data != null && battery_data.moveToFirst()) {
            tmp.setStatus(battery_data.getInt(
                    battery_data.getColumnIndex(Battery_Provider.Battery_Data.STATUS)));

            try {
                Dao<BatteryData, Long> batteryDao = databaseHelper.batteryDao();
                batteryDao.create(tmp);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void BluetoothData(){
        ArrayList<BluetoothData> bluetoothDataArrayList = new ArrayList<>();
        ArrayList<String> added = new ArrayList<>();
        Cursor bluetooth_data = getContentResolver()
                .query(Bluetooth_Provider.Bluetooth_Data.CONTENT_URI,
                        null, "timestamp > " + time , null, "");
        if(bluetooth_data != null && bluetooth_data.moveToFirst()) {
            do{
                String name = bluetooth_data.getString(
                        bluetooth_data.getColumnIndex(Bluetooth_Provider.Bluetooth_Data.BT_NAME));
                if(name != null && !added.contains(name) && !name.equals("")){
                    added.add(name);
                    BluetoothData tmp = new BluetoothData();
                    tmp.setDevice_id(deviceId);
                    tmp.setTimestamp(time);
                    tmp.setNotification_id(notificationId);
                    tmp.setName(name);
                    bluetoothDataArrayList.add(tmp);
                    Log.d(TAG, "Bt name " + tmp.getName());
                }
            }while (bluetooth_data.moveToNext());

            try {
                Dao<BluetoothData, Long> bluetoothDao = databaseHelper.bluetoothDao();
                bluetoothDao.create(bluetoothDataArrayList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    private void putWiFiData(){
        ArrayList<WiFiData> wiFiDataArrayList = new ArrayList<>();
        String[] projection = { "DISTINCT " + WiFi_Provider.WiFi_Data.SSID};
        Cursor wifi_data = getContentResolver()
                .query(WiFi_Provider.WiFi_Data.CONTENT_URI,
                        null, "timestamp > " + time +
                                " AND ssid IS NOT NULL AND ssid != ''",
                        null, "");
        if(wifi_data != null && wifi_data.moveToFirst()) {
            do{
                WiFiData tmp = new WiFiData();
                tmp.setDevice_id(deviceId);
                tmp.setTimestamp(time);
                tmp.setNotification_id(notificationId);
                tmp.setSsid(wifi_data.getString(
                        wifi_data.getColumnIndex(WiFi_Provider.WiFi_Data.SSID)));
                wiFiDataArrayList.add(tmp);
            }while (wifi_data.moveToNext());

            try {
                Dao<WiFiData, Long> wiFiDao = databaseHelper.wifiDao();
                wiFiDao.create(wiFiDataArrayList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void broadcast(int i){
        Log.d(TAG, "Sending Broadcast");
        Intent notify = new Intent(Constants.BROADCAST_DETECTED_ACTIVITY);
        notify.putExtra(Constants.SENSORS_DIS, i);
        LocalBroadcastManager.getInstance(AwareService.this).sendBroadcast(notify);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Sensor sensor = sensorEvent.sensor;
        if (sensor.getType() == Sensor.TYPE_PROXIMITY && !cProx) {
            proxV = sensorEvent.values[0];
            cProx = true;

        }else if (sensor.getType() == Sensor.TYPE_LIGHT) {
            lightV = sensorEvent.values[0];
            cLight = true;
        }
        if(cProx && cLight)
            mSensorManager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private void putUser(){
        AnonId anon = new AnonId();
        anon.setDevice_id(deviceId);
        anon.setHardware_id(Secure.getString(this.getContentResolver(),
                Secure.ANDROID_ID));
        Log.d(TAG, "Hardware id " + anon.getHardware_id());
        try {
            Dao<AnonId, Long> anonDao = databaseHelper.anonIdDao();
            anonDao.create(anon);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public boolean isBluetoothEnabled() {
        final BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        return (bluetoothAdapter != null && bluetoothAdapter.isEnabled());
    }

    public  boolean isLocationEnabled(){
        LocationManager lm = (LocationManager)this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        boolean network_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}
        return  gps_enabled;
    }

    public void getRecentActivity(Context context, long time) {
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        UsageEvents usageEvents = mUsageStatsManager.queryEvents(time - 1000, System.currentTimeMillis());
        UsageEvents.Event event = new UsageEvents.Event();
        boolean found = false;
        Log.d(TAG, "Event noti pck: " + notificationPck);
        if(!notificationReason.equals("Clicked") && !notificationReason.equals("Posted")) {
            notificationReason = "Removed";
            do {
                Log.d(TAG, "Event: " + event.getPackageName() + " Type: " + (event.getEventType() == 1 ? "Foreground" : event.getEventType()));
                if(event.getPackageName() != null)
                    Log.d(TAG, "ASD " + (MainHelper.specialFilter(event.getPackageName(), 8)
                        && event.getEventType() == 12));
                if (event != null && !TextUtils.isEmpty(event.getPackageName())
                        && event.getPackageName() != null && event.getPackageName().equals(notificationPck)
                        && (event.getEventType() == UsageEvents.Event.MOVE_TO_FOREGROUND
                        || (MainHelper.specialFilter(event.getPackageName(), 8)
                        && event.getEventType() == 12))) {
                    notificationReason = "Clicked";
                    int total = sharedPreferences.getInt(Constants.NOTI_ANSW, 0) + 1;
                    sharedPreferences.edit().putInt(Constants.NOTI_ANSW, total).apply();
                    found = true;
                    break;
                }
            } while (usageEvents.getNextEvent(event));
        }

        if(!found && !notificationReason.equals("Posted")){
            int total = sharedPreferences.getInt(Constants.NOTI_RMV, 0) + 1;
            sharedPreferences.edit().putInt(Constants.NOTI_RMV, total).apply();
        }
        Log.d(TAG, "Event: Noti " + notificationReason);
        setSessionData();
    }


}

